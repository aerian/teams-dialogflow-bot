# Teams Dialogflow Bot

This bot has been created using [Bot Framework](https://dev.botframework.com), it shows how to create a simple bot that communicate with
Google Dialogflow and details the deployment of the bot on Teams.

## Prerequisites

### Create a Python virtual environment

- Install Python 3.7
- ```virtualenv -p `which python3.7` venv```
- Create you dialogflow agent on https://dialogflow.cloud.google.com
- Go check the service account of your Dialogflow agent
- Create a JSON API key file
- Access to Azure console and a subscription

## Configuration
- Copy the config.py.template to config.py
- Copy your credentials JSON inside the teams-dialogflow-bot directory
- Modify the values in config.py to fit your settings

## Running the sample
- Run `pip install -r requirements.txt` to install all dependencies
- Run `python app.py`

## Testing the bot using Bot Framework Emulator

[Bot Framework Emulator](https://github.com/microsoft/botframework-emulator) is a desktop application that allows bot developers to test and debug their bots on localhost or running remotely through a tunnel.

- Install the Bot Framework Emulator version 4.3.0 or greater from [here](https://github.com/Microsoft/BotFramework-Emulator/releases)

### Connect to the bot using Bot Framework Emulator

- Launch Bot Framework Emulator
- Enter a Bot URL of `http://localhost:3978/api/messages`

## Deployment

Once you want to deploy the bot on Azure, please install Azure CLI and execute the following commands (The example given will use the free tier):
- Login to Azure `az login`
- List account available if needed `az account list`
- Create Azure Active Directory application `az ad app create --display-name "teams-dialogflow-bot" --password "<YOUR_PASSWORD>" --available-to-other-tenant`
- Retrieve the App ID from previous command
- Create a resource group in Azure interface
- Go into the subdirectory `cd teams-dialogflow-bot`
- Create app service, app service plan `az group deployment create --resource-group "<name-of-resource-group>" --template-file "deploymentTemplates/template-with-preexisting-rg.json" --parameters appId="<app-id-from-previous-step>" appSecret="<password-from-previous-step>" botId="teams-dialogflow-bo" newWebAppName="teams-dialogflow-bot" newAppServicePlanName="teams-dialogflow-bot-plan" appServicePlanLocation="westeurope" --name "teams-dialogflow-bot"`
- Please make sure that all the variables are updated in the config.py (HOST, APP_ID, APP_PASSWORD, DIALOGFLOW_PROJECT_ID)
- Create a Zip of your code `zip -r bot.zip .`
- Deploy the bot `az webapp deployment source config-zip --resource-group "teams-dialogflow-bot" --name "teams-dialogflow-bot" --src bot.zip`

## Further reading

- [Dialogflow Documentation](https://cloud.google.com/dialogflow/docs/tutorials)
- [Bot Framework Documentation](https://docs.botframework.com)
- [How to create a bot](https://docs.microsoft.com/fr-fr/azure/bot-service/bot-builder-tutorial-basic-deploy?view=azure-bot-service-4.0&tabs=python%2CPython)
- [Azure CLI](https://docs.microsoft.com/cli/azure/?view=azure-cli-latest)
- [Azure Portal](https://portal.azure.com)
- [Language Understanding using LUIS](https://docs.microsoft.com/azure/cognitive-services/luis/)

## Additional credits
- [Dialogflow source code example](https://medium.com/swlh/working-with-dialogflow-using-python-client-cb2196d579a4)