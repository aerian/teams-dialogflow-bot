import dialogflow
from google.api_core.exceptions import InvalidArgument
from config import DefaultConfig

CONFIG = DefaultConfig()


def send_intent(intent_text):
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(
        CONFIG.DIALOGFLOW_PROJECT_ID, CONFIG.SESSION_ID
    )
    text_input = dialogflow.types.TextInput(
        text=intent_text, language_code=CONFIG.DIALOGFLOW_LANGUAGE_CODE
    )
    query_input = dialogflow.types.QueryInput(text=text_input)
    try:
        response = session_client.detect_intent(
            session=session, query_input=query_input
        )
    except InvalidArgument:
        raise

    response_text = """
    Query text: %s
    Detected intent: %s
    Detected intent confidence: %s
    Fulfillment text: %s
    """ % (
        response.query_result.query_text,
        response.query_result.intent.display_name,
        response.query_result.intent_detection_confidence,
        response.query_result.fulfillment_text,
    )
    print(response_text)
    return response.query_result.fulfillment_text
